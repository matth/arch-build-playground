# Maintainer: Matt Harrison <matt@harrison.us.com>
# Maintained at: https://github.com/matt-h/aur-pkgbuilds

pkgname=polar-bookshelf
pkgver=1.0.0.beta170
_git_pkgver=1.0.0-beta170
pkgrel=1
pkgdesc="A personal knowledge repository for PDF and web content supporting offline-first caching, annotations, and flashcards."
arch=('x86_64' 'i686')
url="https://getpolarized.io/"
license=('GPL')
depends=()
makedepends=('npm' 'typescript')
source=(
	"${pkgname}-${pkgver}.tar.gz::https://github.com/burtonator/$pkgname/archive/v$_git_pkgver.tar.gz"
	"polar-bookshelf.desktop"
)
sha256sums=(
	'db5af2c6d29d777fa4b371f6512b83fe505976f14dec450383873192ea379e26'
	'6803f2ec6fa6a1458144e73626d284aabe59232e57d4678070af242495503252'
)

build() {
  cd "$srcdir/$pkgname-$_git_pkgver"
  # Install Node packages
  npm install
  # Compile TypeScript
  tsc
  # TODO: Figure out how to make this run with native electron
  # Build electron bundle.
  ./node_modules/electron-builder/out/cli/cli.js --config=electron-builder.yml --linux dir --publish never
}

package() {
  # Install resource files
  install -dm 755 "$pkgdir"/usr/lib/
  cp -r --no-preserve=ownership --preserve=mode "$srcdir/$pkgname-$_git_pkgver/dist/linux-unpacked" "$pkgdir"/usr/lib/$pkgname

  install -dm 755 "$pkgdir"/usr/bin/
  ln -s /usr/lib/$pkgname/polar-bookshelf "$pkgdir"/usr/bin/polar-bookshelf

  # Install icons
  install -Dm 644 "$srcdir/$pkgname-$_git_pkgver/build/icons/16.png" "$pkgdir"/usr/share/icons/hicolor/16x16/apps/polar-bookshelf.png
  install -Dm 644 "$srcdir/$pkgname-$_git_pkgver/build/icons/32.png" "$pkgdir"/usr/share/icons/hicolor/32x32/apps/polar-bookshelf.png
  install -Dm 644 "$srcdir/$pkgname-$_git_pkgver/build/icons/128.png" "$pkgdir"/usr/share/icons/hicolor/128x128/apps/polar-bookshelf.png
  install -Dm 644 "$srcdir/$pkgname-$_git_pkgver/build/icons/256.png" "$pkgdir"/usr/share/icons/hicolor/256x256/apps/polar-bookshelf.png
  install -Dm 644 "$srcdir/$pkgname-$_git_pkgver/build/icons/512.png" "$pkgdir"/usr/share/icons/hicolor/512x512/apps/polar-bookshelf.png
}
